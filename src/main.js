import { createApp } from 'vue'
import { createStore } from 'vuex'
import App from './App.vue'

const store = createStore({
    state() {
        return {
            activeFilter: 'all',
            filters: [
                {
                  value: 'all',
                  label: 'All'
                },
                {
                  value: 'completed',
                  label: 'Completed'
                },
                {
                  value: 'active',
                  label: 'Active'
                },
                {
                  value: 'deleted',
                  label: 'Deleted'
                }
            ],
            items: [
                {
                  name: 'delectus aut autem',
                  status: 'active',
                },
                {
                  name: 'quis ut nam facilis et officia qui',
                  status: 'active',
                },
                {
                  name: 'fugiat veniam minus',
                  status: 'active',
                },
                {
                  name: 'et porro tempora',
                  status: 'completed',
                },
                {
                  name: 'laboriosam mollitia et enim quasi adipisci quia provident illum',
                  status: 'deleted',
                }
            ]
        }
    },
    mutations: {
        setFilterValue(state, status) {
            state.activeFilter = status
        },
        changeItemStatus(state, item) {
            if ('deleted' !== item.status) {
                item.status = ('active' === item.status) ? 'completed' : 'active'
            }
        },
        addItem(state, value) {
            let item = {
                name: value,
                status: 'active',
            }
            state.items.push(item)
        },
        deleteItems(state) {
            for (let i in state.items) {
                if ('completed' === state.items[i].status) {
                    state.items[i].status = 'deleted'
                }
            }
        },
        removeItem(state, value) {
            state.items = state.items.filter(item => item !== value)
        }
    },
    getters: {
        filteredItems(state) {
            if ('all' !== state.activeFilter) {
              return state.items.filter((item) => item.status === state.activeFilter)
            }
            return state.items.filter((item) => item.status !== 'deleted')
        },
        completedItems(state) {
            return state.items.filter((item) => item.status === 'completed');
        },
        getFilters(state) {
            return state.filters
        }
    }
})

const app = createApp(App)
app.use(store)
app.mount('#app')
